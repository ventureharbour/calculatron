const request = require("supertest");
const app = require("../app");
const assert = require("assert");

describe("GET /subtraction", () => {
  it("responds with correct number from ints", () =>
    request(app)
      .get("/subtraction/400/200")
      .expect(200)
      .then(response => {
        assert.equal(response.body.answer, 200);
      }));
  // Default `parseInt` rounding down is good
  it("responds with correct number from floats", () =>
    request(app)
      .get("/subtraction/400/199.7")
      .expect(200)
      .then(response => {
        assert.equal(response.body.answer, 201);
      }));
  it("responds with negative value", () =>
    request(app)
      .get("/subtraction/400/600")
      .expect(200)
      .then(response => {
        assert.equal(response.body.answer, -200);
      }));
  it("responds with silly numbers", () =>
    request(app)
      .get("/subtraction/123456789123456789/123456789123456789")
      .expect(200)
      .then(response => {
        assert.equal(response.body.answer, 0);
      }));
});
