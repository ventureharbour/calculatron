const request = require("supertest");
const app = require("../app");
const assert = require("assert");

describe("GET /addition", () => {
  it("responds with correct number from ints", () =>
    request(app)
      .get("/addition/400/200")
      .expect(200)
      .then(response => {
        assert.equal(response.body.answer, 600);
      }));
  // Default `parseInt` rounding down is good
  it("responds with correct number from floats", () =>
    request(app)
      .get("/addition/400/199.7")
      .expect(200)
      .then(response => {
        assert.equal(response.body.answer, 599);
      }));
  it("responds with silly numbers", () =>
    request(app)
      .get("/addition/123456789123456789/123456789123456789")
      .expect(200)
      .then(response => {
        assert.equal(response.body.answer, 246913578246913570);
      }));
});
